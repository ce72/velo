#!/bin/sh
pylint -d C0103,R0903 --max-line-length=120 setup.py velopy.py
pytest
jupyter-nbconvert --to notebook --output-dir build/ --execute *.ipynb

rm -rf *.egg-info/
rm -rf dist/

python setup.py sdist
python setup.py bdist_wheel
twine check dist/*
twine upload --config-file .pypirc dist/*

pip install velopy --upgrade