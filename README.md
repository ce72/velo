# velo.py

[View it on Gitlab](https://gitlab.com/ce72/velo/-/tree/master)

## About
Library for simple calculations with respect to road cycling and training.

Full documentation with lots of examples available in [jupyter notebook](https://gitlab.com/ce72/velo/-/blob/master/velopy_doc.ipynb).

## Installation
```shell
pip install velopy
```
To use without installing just copy velopy.py to your local directory.

## Usage
Simply import and specify your own FTP which might differ from the default value (290 W). 
```python
import velopy
velopy.FTP = 250

help(velopy)
```

