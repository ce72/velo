import unittest

from assertpy import assert_that

import velopy


class TestVeloFtpMath(unittest.TestCase):

    def setUp(self):
        velopy.FTP = 250

    def test_does_return_percent_of_ftp(self):
        assert_that(velopy.ftp_watts(250)).is_equal_to(100)

    def test_does_return_percent_of_map(self):
        assert_that(velopy.maximum_aerobic_power()).is_equal_to(250 / 0.85)
        assert_that(velopy.map_watts(294)).is_close_to(100, 0.5)

    def test_does_return_training_zone(self):
        velopy.FTP = 290
        assert_that(velopy.zone_watts(900)).is_equal_to(7)
        assert_that(velopy.zone_watts(320)).is_equal_to(5)
        assert_that(velopy.zone_watts(290)).is_equal_to(4)
        assert_that(velopy.zone_watts(200)).is_equal_to(2)
        assert_that(velopy.zone_watts(100)).is_equal_to(1)

    def test_does_return_sst_watt_zone(self):
        assert_that(velopy.sst_watt_zone()).is_equal_to([220, 235])

    def test_does_show_watts(self):
        assert_that(velopy.show_watts(300)).is_equal_to(['L6', '120.0 %FTP', '102.0 %MAP'])

    def test_watts_for_percent_ftp(self):
        assert_that(velopy.watts_ftp(100)).is_equal_to(250)

    def test_watts_for_percent_map(self):
        assert_that(velopy.watts_map(85)).is_equal_to(250)

    def test_vo2max_estimation(self):
        assert_that(velopy.vo2_max(180, 125, 70, 50, 175)).is_close_to(57, 1)


class TestWorkout(unittest.TestCase):

    def test_calculate_kjoule(self):
        assert_that(velopy.kjoule_workout((60, 100))).is_equal_to(360)
        assert_that(velopy.kjoule_workout((60, 200))).is_equal_to(720)
        assert_that(velopy.kjoule_workout((120, 100))).is_equal_to(720)
        assert_that(velopy.kjoule_workout((30, 100, 30, 100))).is_equal_to(360)
        assert_that(velopy.kjoule_workout((30, 100, 30, 100, 30, 200))).is_equal_to(720)

    def test_calculate_kcal(self):
        assert_that(velopy.kcal_workout((60, 100))).is_close_to(374, 1)
        assert_that(velopy.kcal_workout((60, 150))).is_close_to(560, 1)

    def test_calculate_average_power(self):
        assert_that(velopy.avgp_workout((60, 100))).is_equal_to(100)
        assert_that(velopy.avgp_workout((60, 100, 15, 200))).is_equal_to(120)

    def test_calculate_tss(self):
        assert_that(velopy.tss_workout((60, velopy.FTP))).is_equal_to(100)
        assert_that(velopy.tss_workout((60, velopy.FTP, 60, velopy.FTP * 0.85))).is_equal_to(172.25)


class TestPmc(unittest.TestCase):
    def setUp(self):
        velopy.FTP = 250

    def test_calculate_pmc(self):
        actual = velopy.pmc(100, 100, (0, 100, 0, 100, 0, 100), 7).tolist()
        #    TSS, CTL,   ATL,   diff
        assert_that(actual).is_equal_to(
            [[0.0, 100.0, 100.0, 0.0], [100.0, 100.0, 100.0, 0.0],
             [0.0, 97.64716866522433, 86.68778997501816, 10.959378690206165],
             [100.0, 97.70252681812333, 88.45993933251043, 9.242587485612901],
             [0.0, 95.40375115227891, 76.68396642059514, 18.719784731683774],
             [100.0, 95.51189313539237, 79.7878457801808, 15.724047355211567],
             [0.0, 93.2646593852654, 69.16632017551453, 24.098339209750876]]
        )

    def test_calculate_pmc_decay(self):
        actual = velopy.pmc(100, 100).tolist()
        #    TSS, CTL,   ATL,   diff
        assert_that(actual).is_equal_to(
            [[0.0, 100.0, 100.0, 0.0], [0.0, 97.64716866522433, 86.68778997501816, 10.959378690206165],
             [0.0, 95.34969548334767, 75.14772930752859, 20.20196617581908],
             [0.0, 93.10627797040227, 65.14390575310556, 27.962372217296718],
             [0.0, 90.9156442876713, 56.47181220077593, 34.44383208689537],
             [0.0, 88.77655252065779, 48.95416595569531, 39.82238656496248],
             [0.0, 86.68778997501816, 42.437284567695, 44.25050540732316],
             [0.0, 84.64817248906141, 36.787944117144235, 47.860228371917174]]
        )


if __name__ == '__main__':
    unittest.main()
